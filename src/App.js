import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Projeto exemplo - Aula Controle de versão
          Atividade prática - 28/09
        </p>
        <span>Cláudia Sampedro</span>
        <span>Kevin Matos</span>
      </header>
    </div>
  );
}

export default App;
